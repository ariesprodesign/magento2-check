<?php
/**
 * Magento2 Check
 *
 * @copyright    Copyright © 2016 Aries Prodesign (http://ariesprodesign.de/)
 * @author       Aries Prodesign. Eduardo Garces Hernandez
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version     Release: 1.0.0
 */

function checkPopulateRawPostSetting()
{
    // HHVM and PHP 7does not support 'always_populate_raw_post_data' to be set to -1
    if (version_compare(PHP_VERSION, '7.0.0-beta') >= 0 || defined('HHVM_VERSION')) {
        return [];
    }

    $error = false;
    $iniSetting = intval(ini_get('always_populate_raw_post_data'));
    if ($iniSetting !== -1) {
        $error = true;
    }

    $message = sprintf(
        'Your PHP Version is %s, but always_populate_raw_post_data = %d.
 	        $HTTP_RAW_POST_DATA is deprecated from PHP 5.6 onwards and will be removed in PHP 7.0.
 	        This will stop the installer from running.
	        Please open your php.ini file and set always_populate_raw_post_data to -1.
 	        If you need more help please call your hosting provider.',
        PHP_VERSION,
        intval(ini_get('always_populate_raw_post_data'))
    );

    $data = [
        'message' => $message,
        'helpUrl' => 'http://php.net/manual/en/ini.core.php#ini.always-populate-settings-data',
        'error' => $error
    ];

    return $data;
}

$memory_limit = ini_get('memory_limit');
if (preg_match('/^(\d+)(.)$/', $memory_limit, $matches)) {
    if ($matches[2] == 'M') {
        $memory_limit = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
    } else if ($matches[2] == 'K') {
        $memory_limit = $matches[1] * 1024; // nnnK -> nnn KB
    }
}

$memory_limit_ok = ($memory_limit >= 512 * 1024 * 1024); // at least 64M?

$always_populate_raw_post_data = checkPopulateRawPostSetting();
$curl = extension_loaded('curl');
$dom = extension_loaded('dom');
$iconv = extension_loaded('iconv');
$mcrypt = extension_loaded('mcrypt');
$simplexml = extension_loaded('simplexml');
$spl = extension_loaded('spl');
$xsl = extension_loaded('xsl');
$intl = extension_loaded('intl');
$mbstring = extension_loaded('mbstring');
$ctype = extension_loaded('ctype');
$hash = extension_loaded('hash');
$openssl = extension_loaded('openssl');
$zip = extension_loaded('zip');
$xmlwriter = extension_loaded('xmlwriter');
$tokenizer = extension_loaded('tokenizer');
$gd = extension_loaded('gd');



?>

<html>
<head>
    <style>
        .container{
            max-width: 900px;
            width: 100%;
            margin: 20px auto;
            overflow: hidden;
            padding: 10px;
        }
        .icon-success::before {
            content: "Ok!";
        }
        .icon-failed::before {
            content: "Error!";
        }
        .icon-failed, .icon-failed::before{
            color: crimson;
        }
        .icon-success, .icon-success::before{
            color: #398439;
        }
        .icon-failed::before, .icon-success::before{
            font-style: normal;
            font-weight: 400;
            line-height: 1;
        }
        .list > li {
            display: block;
            margin-bottom: 0.75em;
            position: relative;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Is your server compatible with Magento 2?</h1>
    <hr>
    <?php
    if(!$curl || !$dom  || !$iconv  || !$mcrypt  || !$simplexml  || !$spl  || !$xsl  || !$intl  || !$mbstring || !$ctype || !$hash || !$openssl || !$zip || !$xmlwriter || !$tokenizer
        || !$gd || !$memory_limit_ok || (isset($always_populate_raw_post_data['error']) && $always_populate_raw_post_data['error']) || version_compare(phpversion(), '5.5.22', '>=')!==true) {
        echo '<p><strong>Your server does not meet the following requirements in order to install Magento 2.</strong>';
        echo '<br>The following requirements failed, please contact your hosting provider in order to receive assistance with meeting the system requirements for Magento:';
    } else {
        echo '<p><strong>Congratulations!</strong> Your server meets the requirements for Magento 2.</p>';
    }
    ?>
    <hr>
    <div class="readiness-check-item ng-scope" id="php-version">
        <div>
            <h3 class="readiness-check-title">Checking PHP Version...</h3>
        </div>
        <?php if(version_compare(phpversion(), '5.5.22', '>=')===true):?>
            <div ng-switch="version.responseType" class="">
                <div class="ng-scope">
                    <span class="readiness-check-icon icon-success"></span>
                    <div class="readiness-check-content">
                        <p class="ng-binding">
                            Your PHP version is correct (<?php echo phpversion()?>).
                        </p>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div ng-switch="version.responseType" class="">
                <div class="ng-scope">
                    <span class="readiness-check-icon icon-failed"></span>
                    <div class="readiness-check-content">
                        <p class="ng-binding">
                            Your PHP version is not correct (<?php echo phpversion()?>). You need<strong> PHP 5.5.22</strong> (or greater)
                        </p>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
    <hr>
    <?php if(isset($always_populate_raw_post_data) && is_array($always_populate_raw_post_data) && isset($always_populate_raw_post_data['error']) && $always_populate_raw_post_data['error']):?>
        <div class="readiness-check-item ng-scope">
            <div class="readiness-check-content">
                <h3 class="readiness-check-title">PHP Settings Check *</h3>
                <span class="readiness-check-icon icon-failed"></span>
                <div class="">
                    <p class="ng-binding">
                        <?php echo $always_populate_raw_post_data['message'] ?>
                    </p>
                </div>
                <div class="ng-scope">
                    <div class="readiness-check-side">
                        <p class="side-title">Need Help?</p>
                        <a target="_blank" href="http://php.net/manual/en/ini.core.php#ini.always-populate-settings-data">PHP Documentation</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    <?php endif ?>
    <div class="readiness-check-item ng-scope" id="php-version">
        <div>
            <h3 class="readiness-check-title">Checking RAM...</h3>
        </div>
        <?php if($memory_limit_ok):?>
            <div ng-switch="version.responseType" class="">
                <div class="ng-scope">
                    <span class="readiness-check-icon icon-success"></span>
                    <div class="readiness-check-content">
                        <p class="ng-binding">
                            Your RAM is correct (<?php echo ini_get('memory_limit')?>).
                        </p>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div ng-switch="version.responseType" class="">
                <div class="ng-scope">
                    <span class="readiness-check-icon icon-failed"></span>
                    <div class="readiness-check-content">
                        <p class="ng-binding">
                            Your RAM is not correct (<?php echo ini_get('memory_limit')?>). The application requires at least 512M
                        </p>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
    <hr>
    <h3 class="readiness-check-title">PHP Extensions Check</h3>
    <ul class="list">
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($curl) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension curl.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($dom) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension dom.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($mcrypt) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension mcrypt.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($simplexml) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension simplexml.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($spl) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension spl.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($xsl) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension xsl.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($intl) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-when="true"></span>
            PHP Extension intl.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($mbstring) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension mbstring.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($ctype) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension ctype.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($hash) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension hash.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($openssl) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension openssl.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($zip) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension zip.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($xmlwriter) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension xmlwriter.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($tokenizer) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension tokenizer.
        </li>
        <li class="list-item-icon ng-scope ng-binding">
            <span class="<?php if($gd) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
            PHP Extension gd.
        </li>
        <?php if(version_compare(phpversion(), '5.5.22', '>=')===true):?>
            <li class="list-item-icon ng-scope ng-binding">
                <span class="<?php if(extension_loaded('json')) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
                PHP Extension json.
            </li>
            <li class="list-item-icon ng-scope ng-binding">
                <span class="<?php if(extension_loaded('iconv')) echo 'icon-success'; else echo 'icon-failed'; ?> ng-scope" ng-switch-default=""></span>
                PHP Extension iconv.
            </li>
        <?php endif ?>
    </ul>
    <hr>


    <div>
        More Info: <a href="http://devdocs.magento.com/guides/v2.0/install-gde/system-requirements.html">http://devdocs.magento.com/guides/v2.0/install-gde/system-requirements.html</a>
    </div>


</div>
</body>
</html>

